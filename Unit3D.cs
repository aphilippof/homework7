﻿namespace HomeWork7
{
    public class Unit3D : Unit
    {
        private BoxCollider unitСollider;

        // в параметрах можно в base прокидывать, что нам нужно в родительском классе инициализировать
        public Unit3D(BoxCollider boxCollider) : base()
        {
            unitCollider = boxCollider;
            // ну или не в параметрах collider прокидывать, а так искать
        }

        public override void EnemyCollision()
        {
            // тут работаем с unitCollider в 3D контексте
        }
        // и т.д.
    }
}
