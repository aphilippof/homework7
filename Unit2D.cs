﻿namespace HomeWork7
{
    public class Unit2D : Unit
    {
        private Collider2D unitCollider;

        // в параметрах можно в base прокидывать, что нам нужно в родительском классе инициализировать
        public Unit2D(Collider2D collider2D) : base()
        {
            unitCollider = collider2D;
            // ну или не в параметрах collider прокидывать, а так искать
        }

        public override void EnemyCollision()
        {
            // тут работаем с unitCollider в 2D контексте
        }
        // и т.д.
    }
}
