﻿namespace HomeWork7
{
    public abstract class Unit
    {
        // общая часть для Unit2D и Unit3D
        // методы, свойства, события

        public Unit()
        {
            // конструктор инициализирует общую часть Unit2D и Unit3D
        }

        // группа абстрактных методов, применяющих collider
        // например:
        public abstract void EnemyCollision();
        // и т.д.
    }
}
